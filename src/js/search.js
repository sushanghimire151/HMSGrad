//Search Modal
var srchModal = document.getElementById("searchModal");
var srchBtn = document.getElementById("searchBtn");
var span = document.getElementsByClassName("close")[0];

srchBtn.onclick = function() {
  srchModal.style.display = "block";
}

span.onclick = function() {
  srchModal.style.display = "none";
}

window.onclick = function(event) {
  if (event.target == srchModal) {
    srchModal.style.display = "none";
  }
}